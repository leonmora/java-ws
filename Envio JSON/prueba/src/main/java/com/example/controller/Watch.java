package com.example.controller;

public class Watch {

	public String brand;
	public String style;
	public String mechanismType;
	
	public Watch(String brand, String style, String mechanismType) {
		this.brand = brand;
		this.style = style;
		this.mechanismType = mechanismType;
	}
	
	public void setBrand(String brand){
		this.brand = brand;
	}
	
}
