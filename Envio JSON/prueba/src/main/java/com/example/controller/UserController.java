package com.example.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.http.*;

@RestController
public class UserController {
	 
	@GetMapping("/getInfo")
	public String getUserDetails() {
		return "saludos desde el ws";
	}
	
	@RequestMapping(value = "/test")
	public ResponseEntity<Watch> get(){
		Watch brandNewWatch = new Watch("Seiko", "Buceo", "Cuarzo");
		
		return new ResponseEntity<Watch>(brandNewWatch, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/postingTest", method = RequestMethod.POST)
	public ResponseEntity<Watch> update(@RequestBody Watch littleWatch){
		
		if(littleWatch != null) {
			littleWatch.setBrand("Grand"+littleWatch.brand); 
		}
		
		return new ResponseEntity<Watch>(littleWatch, HttpStatus.OK);
		
	}
}
